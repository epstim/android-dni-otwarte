package com.epstim.rss.reader;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alan Żur on 2014-05-19.
 */
public class RSSHandler{
    List<Article> articles;
    URL url;

    public RSSHandler(String url) throws MalformedURLException{
        this.url = new URL(url);
        articles = new ArrayList<Article>();
    }

    public List<Article> getArticles() throws XmlPullParserException, IOException {
        List<String> titles = new ArrayList<String>();
        List<String> links = new ArrayList<String>();
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        factory.setNamespaceAware(false);
        XmlPullParser xpp = factory.newPullParser();
        xpp.setInput(getInputStream(), "UTF-8");
        boolean insideItem = false;
        int eventType = xpp.getEventType();
        while(eventType != XmlPullParser.END_DOCUMENT){
            if (eventType == XmlPullParser.START_TAG){
                if (xpp.getName().equalsIgnoreCase("item")){
                    insideItem = true;
                }else if (xpp.getName().equalsIgnoreCase("title")){
                    if (insideItem) titles.add(xpp.nextText());
                }else if (xpp.getName().equalsIgnoreCase("link")){
                    if (insideItem) links.add(xpp.nextText());
                }
            }else if (eventType == XmlPullParser.END_TAG && xpp.getName().equalsIgnoreCase("item")){
                insideItem = false;
            }

            eventType = xpp.next();
        }

        for(int i = 0 ; i < titles.size() ; i++){
            articles.add(new Article(titles.get(i), links.get(i)));
        }

        return articles;
    }

    private InputStream getInputStream(){
        try {
            return url.openConnection().getInputStream();
        } catch (IOException e) {
            return null;
        }
    }
}
