package com.epstim.rss.reader;

/**
 * Created by Alan Żur on 2014-05-19.
 */
public class Article {
    private String title;
    private String link;

    public Article(String title, String link){
        this.title = title;
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String toString(){
        return this.getTitle();
    }
}
