package com.epstim.rss.reader;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

/**
 * Created by Alan Żur on 2014-05-19.
 */
public class AlertDialogFragment extends DialogFragment {

    public static AlertDialogFragment newInstance(){
        return new AlertDialogFragment();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity()).setTitle("Loading...").setMessage("Please wait").create();
    }
}
